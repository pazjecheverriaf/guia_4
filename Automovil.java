import java.util.ArrayList;

public class Automovil {
	
	private Motor motor;
	private Velocimetro velocimetro;
	private boolean encendido;
	private Estanque estanque;
	private ArrayList<Ruedas> rueda;
	private double distancia_recorrida = 0;
	private double distancia;
	private double consumo; 
	private double velocidad;
	private int tiempo;
	private int sin_combustible;
	
	Automovil(){
		this.rueda = new ArrayList<Ruedas>();
		this.distancia_recorrida = 0;
	}
	
	public Motor getMotor() {
		return motor;
	}
	
	
	public void setMotor(Motor motor) {
		this.motor = motor;
	}


	public Velocimetro getVelocimetro() {
		return velocimetro;
	}


	public void setVelocimetro(Velocimetro velocimetro) {
		this.velocimetro = velocimetro;
	}


	public Estanque getEstanque() {
		return estanque;
	}


	public void setEstanque(Estanque estanque) {
		this.estanque = estanque;
	}


	public boolean getEncendido() {
		return encendido;
		
	}


	public void setEncendido(boolean encendido) {
		this.encendido = encendido;
	}


	public ArrayList<Ruedas> getRueda() {
		return rueda;
	}


	public void setRueda(Ruedas rueda) {
		this.rueda.add(rueda);
	}

	
	public void enciende_auto(Estanque estanque) {
		this.encendido = true;
		if( this.encendido) {
			this.encendido = true;
			System.out.println("|AUTO ENCENDIDO|");
			double combustible = estanque.getCombustible();
			estanque.setCombustible(combustible - combustible /100);
			System.out.println("Combustible despues del encendido es: " + estanque.getCombustible()+ "1"); 
			
		}else {
			this.encendido = false;
			System.out.println("|AUTO APAGADO|");
			}
	}
			
			
	
	
	
	public void movimiento() {
		velocidad = (double)(Math.random()*120+1);
		tiempo = (( int)(Math.random()*10+1));
		
		distancia = tiempo * velocidad;
		distancia_recorrida = distancia + distancia_recorrida;
	
		System.out.println(" La distancia recorrida por el automovil es: " + distancia_recorrida);
		
	}
		
	
	public void consumo () {
		//gasto de combustible segun la cilindrada 
		consumo = 0;
	 
		if (motor.getCilindrada()== "1,2") {
			consumo = distancia /20; 
			if (consumo >= estanque.getCombustible()) {
				consumo= estanque.getCombustible();
				double velocidad = distancia/tiempo;
				
			}
			
			consumo = estanque.getCombustible()-consumo;
			estanque.setCombustible(consumo);
		}
		else {
			consumo = distancia/14;
			if (consumo >= estanque.getCombustible()) { 
				
				consumo = estanque.getCombustible();
				distancia = consumo*14;
				double  velocidad = distancia/tiempo;
				velocimetro.setVelocidad(velocidad); 

				}
			consumo =  estanque.getCombustible()-consumo;
			estanque.setCombustible(consumo);


		}			}
	
	public int getSin_combustible() {
		return sin_combustible;
	}

	public void setSin_combustible(int sin_combustible) {
		this.sin_combustible = sin_combustible;
	}
	
	public void revision_ruedas() {
		for (Ruedas r : this.rueda) {
			System.out.println(r.getDesgaste());
		}
	}
	
	
	public void estado_auto() {
		System.out.println("Reporte-Estado del auto:");
		System.out.println("El automovil tiene un motor de "+ this.motor.getCilindrada());
		System.out.println("La velocida del auto es " + this.velocimetro.getVelocidad() );
		System.out.println("La distancia recorrida es: " + distancia_recorrida + "km" );
		System.out.println("El estado de actual del estanque es: " + this.estanque.getCombustible());
		
		revision_ruedas(); 
	}


	
	
	
	

}
