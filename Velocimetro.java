
public class Velocimetro {
	

	private int velocidad_max = 120;
	private double velocidad;
	private int tiempo;
	
	Velocimetro(){
		velocidad = (int)(Math.random()*120+1);
		tiempo =  ( int)(Math.random()*10+1);
	}
	
	public double getVelocidad() {
		return velocidad;
	}
	public void setVelocidad(double velocidad) {
		this.velocidad = velocidad;
	}
	public int getVelocidad_max() {
		return this.velocidad_max;
	}
}	

